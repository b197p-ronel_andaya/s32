const http = require("http");
const port = 4000;
let resources = [
  {
    course: "HTML/CSS JAVASCRIPT",
    description: "What you need to get started",
  },
  {
    course: "NODE JS, EXPRESS, MONGO DB",
    description: "Backend lord",
  },
];
// 1.
const server = http.createServer((req, res) => {
  // 2.
  let response = "";

  res.writeHead(200, { "Content-Type": "text/plain" });
  if (req.url === "/" && req.method === "GET") {
    response = "Welcome to Booking System";
  } else if (req.url === "/profile" && req.method === "GET") {
    response = "Welcome to your profile";
  } else if (req.url === "/courses" && req.method === "GET") {
    response = `Here's our courses available
    ${JSON.stringify(resources)}`;
  } else if (req.url === "/addcourse" && req.method === "GET") {
    let courses = JSON.stringify(resources);
    response = `Add a course to our resources
        ${courses}`;
  } else if (req.url === "/addcourse" && req.method === "POST") {
    let courseBody = "";
    let courseName = '';

    req.on("data", (data) => {
      courseBody += data;
    });

    req.on("end", () => {
      courseBody = JSON.parse(courseBody);
      courseObj = {
        course: courseBody.course,
        description: courseBody.description,
      };

      resources.push(courseObj);
      courseName = courseBody.course;
      
    });
    response = `Course added to resources`;
  } else if (req.url === "/updatecourse" && req.method === "PUT") {
    response = "Update a course to our resources";
  } else if (req.url === "/deletecourse" && req.method === "DELETE") {
    response = "Archive courses to our resources";
  } else {
    res.writeHead(404, { "Content-Type": "text/plain" });
    response = "Error 404. The page you are looking for cannot be found!";
  }

  res.end(response);
});

server.listen(4000);

console.log(`server is now up and running and listening on port ${port}`);
